from graph import Graph, nye
from pyrsistent import v, pmap, ny, inc, rex

# I do not use unittesting framework on purpose here to show that you can do quite a lot without it

g1 = Graph()
assert (g1.hasvertex(1) is False)
g2 = g1.addvertex(1)
assert (g2.hasvertex(1) is True)
assert (g1.hasvertex(1) is False)
assert (g2.hasvertex(2) is False)

g3 = g2.addvertex(2)
g4 = g3.addvertex(3)
assert (g4.hasedge(1, 2) is False)
assert (g4.hasedge(2, 1) is False)
g5 = g4.addedge(2, 3)
assert (g5.hasedge(1, 2) is False)
assert (g5.hasedge(2, 1) is False)
assert (g5.hasedge(3, 2) is True)
assert (g5.hasedge(2, 3) is True)
assert (g4.hasedge(3, 2) is False)
assert (g4.hasedge(2, 3) is False)

assert (set(g5.neighbours(1)) == set())
assert (set(g5.neighbours(2)) == set({3}))
assert (set(g5.neighbours(3)) == set({2}))

g6 = g5.addedge(3, 2)
assert (set(g6.neighbours(1)) == set())
assert (set(g6.neighbours(2)) == set({3}))
assert (set(g6.neighbours(3)) == set({2}))

g7 = g6.addedge(2, 1)
assert (set(g7.neighbours(1)) == set({2}))
assert (set(g7.neighbours(2)) == set({1, 3}))
assert (set(g7.neighbours(3)) == set({2}))

g8 = g7.removeedge(3, 2)
assert (set(g8.neighbours(1)) == set({2}))
assert (set(g8.neighbours(2)) == set({1}))
assert (set(g8.neighbours(3)) == set())

g9 = g7.removevertex(2)
assert (g9.hasvertex(1) is True)
assert (g9.hasvertex(2) is False)
assert (set(g9.neighbours(1)) == set())
assert (set(g9.neighbours(3)) == set())

g10 = g7.removevertex(1)
assert (g10.hasvertex(1) is False)
assert (g10.hasvertex(2) is True)
assert (set(g10.neighbours(2)) == set({3}))
assert (set(g10.neighbours(3)) == set({2}))

g11 = Graph()
g12 = g11.setvertexlabel(0, 1)
assert (g12.hasvertex(0) is False)

g13 = g12.addvertex(5)
g14 = g13.setvertexlabel(5, 4)
assert (g14.hasvertex(5) is True)
assert (g14.hasvertex(4) is False)
assert (g14.getvertexlabel(5) == 4)

g15 = g14.setvertexlabel(5, 2)
assert (g14.hasvertex(5) is True)
assert (g14.hasvertex(4) is False)
assert (g14.hasvertex(2) is False)
assert (g14.hasedge(5, 2) is False)
assert (g15.getvertexlabel(5) == 2)

g16 = g15.addvertex(1)
g17 = g16.addedge(1, 5)
assert (g17.hasvertex(5) is True)
assert (g17.hasvertex(4) is False)
assert (g17.hasvertex(2) is False)
assert (g17.hasedge(1, 5) is True)

g18 = g17.setvertexlabel(5, "test")
assert (g17.hasvertex(5) is True)

g19 = g18.setvertexlabel(1, v(6, 7, 8))
assert (g19.hasvertex(1) is True)
assert (g19.getvertexlabel(1) == v(6, 7, 8))

g20 = g19.addvertex(2)
assert (g20.getvertexlabel(2) is None)

g21 = g20.setedgelabel(1, 5, 100)
assert (g21.hasedge(1, 5) is True)
assert (g21.hasvertex(100) is False)
assert (g21.hasedge(1, 100) is False)
assert (g21.hasedge(5, 100) is False)
assert (g21.getedgelabel(1, 5) == 100)
assert (g21.getedgelabel(5, 1) == 100)

g22 = g21.setedgelabel(1, 5, v(1, 2, 3))
assert (g22.getedgelabel(1, 5) == v(1, 2, 3))
assert (g22.getvertexlabel(1) == v(6, 7, 8))

g23 = g22.removeedge(1, 5)
assert (g23.getedgelabel(1, 5) is None)
assert (g22.getvertexlabel(1) == v(6, 7, 8))

g24 = g22.removevertex(1)
assert (g24.getedgelabel(1, 5) is None)
assert (g24.getvertexlabel(1) is None)
assert (g22.getedgelabel(1, 5) == v(1, 2, 3))
assert (g22.getvertexlabel(1) == v(6, 7, 8))

g25 = g22.setedgelabel(1, 5, v(1, v(5, 6, pmap({'test': 1000})), 2, 3, 4))
assert (g25.transform([], 4) == 4)
assert (g25.transform([], lambda x: x.getvertexlabel(1)) == v(6, 7, 8))
assert (g25.transform([(1, 5), 0], lambda x: x + 1).getedgelabel(1, 5) == v(2, v(5, 6, pmap({'test': 1000})), 2, 3, 4))
assert (g25.transform([(1, 5)], 10).getedgelabel(1, 5) == 10)
assert (g25.transform([1], "a").getvertexlabel(1) == "a")
assert (g25.transform([(1, 5), 1], 4).getedgelabel(1, 5) == v(1, 4, 2, 3, 4))
assert (g25.transform([(1, 5), 1, 2, 'test'], 40).getedgelabel(1, 5) == v(1, v(5, 6, pmap({'test': 40})), 2, 3, 4))
assert (g25.transform([(1, 5), 0], lambda x: x + 1, [(1, 5), 1, 2, 'test'], 40).
        getedgelabel(1, 5) == v(2, v(5, 6, pmap({'test': 40})), 2, 3, 4))

g26 = g22.setvertexlabel(1, v(1, v(5, 6, pmap({'test': 1000})), 2, 3, 4))
assert (g26.transform([1, 0], lambda x: x + 1).getvertexlabel(1) == v(2, v(5, 6, pmap({'test': 1000})), 2, 3, 4))
assert (g26.transform([1], 10).getvertexlabel(1) == 10)
assert (g26.transform([1, 1], 4).getvertexlabel(1) == v(1, 4, 2, 3, 4))
assert (g26.transform([1, 1, 2, 'test'], 40).getvertexlabel(1) == v(1, v(5, 6, pmap({'test': 40})), 2, 3, 4))
assert (g26.transform([1, 0], lambda x: x + 1, [1, 1, 2, 'test'], 40).
        getvertexlabel(1) == v(2, v(5, 6, pmap({'test': 40})), 2, 3, 4))

g27 = g26.addedge(1, 2)
assert (g27.transform([lambda v1, v2: v1 == 1 and v2 < 5], 7)._edges == pmap({v(1, 2): 7, v(1, 5): v(1, 2, 3)}))
assert (g27.transform([lambda v: v == 1 or v == 2], 1000)._vertices ==
        pmap({1: 1000, 2: 1000, 5: 'test'}))
assert (g27.transform([nye], "any edge")._edges == pmap({v(1, 2): "any edge", v(1, 5): "any edge"}))
assert (g27.transform([ny], "any vertex")._vertices ==
        pmap({1: "any vertex", 2: "any vertex", 5: 'any vertex'}))
assert (g27.transform([(5, 1), 1], inc).getedgelabel(1, 5) == v(1, 3, 3))

g28 = g27.setedgelabel(1, 2, 0)
assert (g28.transform([(1, 2)], inc).getedgelabel(1, 2) == 1)
assert (g28.transform([1, 1, 2, rex('^te')], "rex").getvertexlabel(1) == v(1, v(5, 6, pmap({'test': 'rex'})), 2, 3, 4))

v1 = v(g27, g28)
assert (v1.transform([0, (1, 5), 0], "aaa")[0].getedgelabel(1, 5) == v('aaa', 2, 3))
assert (v1.transform([0, (1, 5), 0], lambda x: "bbb")[0].getedgelabel(1, 5) == v('bbb', 2, 3))
assert (v1.transform([0, 1, 1, 1], "aaa")[0].getvertexlabel(1) == v(1, v(5, 'aaa', pmap({'test': 1000})), 2, 3, 4))

g29 = g28.setvertexlabel(1, g18)
assert (g29.transform([1, 5], "www").getvertexlabel(1).getvertexlabel(5) == "www")

g30 = g29.setvertexlabel(1, 'test')
assert (g30.getverticesbylabel('test') == [1,5])
assert (g30.getedgesbylabel(0)==[(1,2)])
assert (g30.getverticesbylabel('test2') == [])


print("Tests complete.")
