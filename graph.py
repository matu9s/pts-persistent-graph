from pyrsistent import pmap, pset, v
import inspect


def nye(v1, v2):
    return True


# you may want to use PClass, but we want to learn something
class Graph:
    def __init__(self):
        self._incidence = pmap()  # map<int,set<int>>
        self._vertices = pmap()  # map<int, label>
        self._edges = pmap()  # map<pvec<int, int>, label>

    def _newgraph(self, incidence, vertices, edges):
        a = Graph()
        a._incidence = incidence
        a._vertices = vertices
        a._edges = edges
        return a

    def hasvertex(self, v):
        return v in self._incidence

    def addvertex(self, v):
        if self.hasvertex(v):
            return self
        else:
            return self._newgraph(self._incidence.set(v, pset()), self._vertices.set(v, None), self._edges)

    def setvertexlabel(self, v, label):
        if self.hasvertex(v):
            return self._newgraph(self._incidence, self._vertices.set(v, label), self._edges)
        else:
            return self

    def getvertexlabel(self, v):
        return self._vertices.get(v)

    def getverticesbylabel(self, label):
        return [vertex for vertex in self._vertices if self.getvertexlabel(vertex) == label]

    def hasedge(self, v1, v2):
        if self.hasvertex(v1):
            return v2 in self._incidence[v1]
        else:
            return False

    def addedge(self, v1, v2):
        addv1 = lambda x: x.add(v1)
        addv2 = lambda x: x.add(v2)
        newinc = self._incidence.transform([v1], addv2, [v2], addv1)
        return self._newgraph(newinc, self._vertices, self._edges.set(v(min(v1, v2), max(v1, v2)), None))

    def setedgelabel(self, v1, v2, label):
        if self.hasedge(v1, v2):
            return self._newgraph(self._incidence, self._vertices, self._edges.set(v(min(v1, v2), max(v1, v2)), label))
        else:
            return self

    def getedgelabel(self, v1, v2):
        return self._edges.get(v(min(v1, v2), max(v1, v2)))

    def getedgesbylabel(self, label):
        return [(edge[0], edge[1]) for edge in self._edges if self.getedgelabel(edge[0], edge[1]) == label]

    def neighbours(self, v):
        for el in self._incidence[v]:
            yield el

    def removeedge(self, v1, v2):
        removev1 = lambda x: x.remove(v1)
        removev2 = lambda x: x.remove(v2)
        newinc = self._incidence.transform([v1], removev2, [v2], removev1)
        return self._newgraph(newinc, self._vertices, self._edges.discard(v(min(v1, v2), max(v1, v2))))

    def removevertex(self, v):
        evovertex = self._incidence.evolver()
        for v2 in self._incidence[v]:
            evovertex[v2] = evovertex[v2].remove(v)
        evovertex.remove(v)
        newedges = self._edges
        for edge in self._edges:
            if edge[0] == v or edge[1] == v:
                newedges = newedges.discard(edge)
        return self._newgraph(evovertex.persistent(), self._vertices.discard(v), newedges)

    def _callabblepath(self, path, transformation, newgraph):
        if len(inspect.signature(path[0]).parameters) == 1:
            for vertex in filter(path[0], self._vertices):
                newgraph = newgraph.transform([vertex] + path[1:], transformation)
        else:
            for edge in self._edges:
                if path[0](min(edge[0], edge[1]), max(edge[0], edge[1])):
                    newgraph = newgraph.transform([(edge[0], edge[1])] + path[1:], transformation)
        return newgraph

    def _uncallablepath(self, path, transformation, newgraph):
        if type(path[0]) is tuple:
            label = newgraph.getedgelabel(path[0][0], path[0][1])
        else:
            label = newgraph.getvertexlabel(path[0])

        if callable(getattr(label, "transform", None)):
            label = label.transform(path[1:], transformation)
        else:
            if callable(transformation):
                label = transformation(label)
            else:
                label = transformation

        if type(path[0]) is tuple:
            newgraph = newgraph.setedgelabel(path[0][0], path[0][1], label)
        else:
            newgraph = newgraph.setvertexlabel(path[0], label)

        return newgraph

    def transform(self, *args):
        newgraph = self
        for path, transformation in zip(*[iter(args)] * 2):
            if len(path) == 0:
                if callable(transformation):
                    newgraph = transformation(newgraph)
                else:
                    newgraph = transformation

            elif len(path) > 0:
                if callable(path[0]):
                    newgraph = self._callabblepath(path, transformation, newgraph)
                else:
                    newgraph = self._uncallablepath(path, transformation, newgraph)

        return newgraph

    def __getitem__(self, item):
        if type(item) is tuple:
            return self.getedgelabel(item[0], item[1])
        else:
            return self.getvertexlabel(item)

    def evolver(self):
        return Graphevolver(self)


class Graphevolver:
    def __init__(self, graph):
        self.graph = graph

    def __setitem__(self, key, value):
        self.graph = self.graph.transform([key], value)

    def persistent(self):
        return self.graph
