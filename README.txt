Persistent Graph


Label
	- Na lable som vytvoril dve nové pmapy (pre hrany a pre vrcholy) aby to bolo prehľadnejšie

Transform
	- Cez transform sa dajú upraviť hrany, vrcholy a zmeniť lable, pomocou priradenia alebo lambda funkcie
	- Transform skontroluje či je prvý prvok path ([] s indexovaním) callable, ak áno podľa toho či je to funkca s dvoma parametrami
	  robí "filter" cez hrany alebo s jedným parametrom robí "filter" cez vrcholy
		-Ak nie je callable, ak je zadaný prvý parameter tuple robí sa transform na hrane, ak int robí sa transform na vrchole 
	-ny spraví zmenu v každom vrchole, nye (musí sa importnuť z graph.py) spraví zmenu v každej hrane, na podštruktúrach fungujú rex a inc
    - funguje aj viacnásobná úprava cez viacero path v jednom transform
    - funguje transform na grafoch vnorených v pvectoroch alebo pmapách ("Try to change the graph class so that it will work
     with other transform functions. That is, if we define a PVector of Graphs such that vertex labels will be PMaps,
      can use transform on PVector to change the PMaps.")